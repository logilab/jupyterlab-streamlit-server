import os
from glob import glob

def setup_streamlit():

    def _streamlit_command(port):
        tool_path = './app/index.py'
        if not os.path.exists(tool_path):
            tool_path = './app/app.py'
            if not os.path.exists(tool_path):
                raise FileNotFoundError(f'Can not find tool file (index.py or app.py)')
        return ['streamlit', 'run', tool_path]

    return {
        'command': _streamlit_command,
        'timeout': 40,
        'port': 8501,
        'new_browser_window': True,
        'launcher_entry': {
            'enabled': False,
            'icon_path': os.path.join(
                os.path.dirname(os.path.abspath(__file__)),
                'icons',
                'streamlit.svg'
            ),
            'title': "Streamlit",
        }
    }
