import setuptools

setuptools.setup(
    name="jupyter-streamlit-server",
    packages=setuptools.find_packages(),
    package_data={
        'jupyter_streamlit_server': ['icons/*'],
    },
    entry_points={
        'jupyter_serverproxy_servers': [
            'jupyter_streamlit_server = jupyter_streamlit_server:setup_streamlit',
        ]
    },
    install_requires=['jupyter-server-proxy>=4.1.0'],
)
